Rails.application.routes.draw do
  resources :users
  resources :user_sessions, only: [:create, :new, :destroy]
  post "user_sessions/new" =>  "user_sessions/new"
  get "user_sessions/loginExterno" =>  "user_sessions/loginExterno"

  root "welcome#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
