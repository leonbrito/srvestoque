class User < ApplicationRecord
	has_secure_password
	validates_presence_of :nome, :password, :password_confirmation

	def self.authenticate(nome, password)
      find_by(nome: nome).
      try(:authenticate, password)
  	end
end
