class UserSessionsController < ApplicationController
  protect_from_forgery prepend: true
  before_action :require_no_authentication, only: [:new, :create]
  before_action :require_authentication, only: :destroy

  def new
    puts params
    @user_session = UserSession.new(session)
    @nome = params[:nome]
    @password = params[:password]
    if !@nome.blank?
      @user_session.nome = @nome
    end

    if !@password.blank? 
      @user_session.password = @password
    end
  end

  def loginExterno
    @user_session = UserSession.new(session, params)
    if @user_session.authenticate! 
      redirect_to root_path, notice: 'Logado com sucesso'
    else
      render :new
    end
  end

  def create
    
    @user_session = UserSession.new(session, params[:user_session])
    if @user_session.authenticate! 
      redirect_to root_path, notice: 'Logado com sucesso'
    else
      render :new
    end
  end

  def destroy
    user_session.destroy
    redirect_to root_path
  end
end